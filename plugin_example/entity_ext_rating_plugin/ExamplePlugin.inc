<?php

//If a parent plugin is specified in the plugin definition, in module, then that
//plugin class should be extended.
class ExamplePlugin extends EntityExtRatingSource {
  public function sourceName() {
    return 'ExamplePlugin';
  }
  
  /**
   * Add a configuration form. Here we will set how many rating points will return
   * this dummy plugin.
   */
  public function configForm(&$form_state) {
    $form = parent::configForm($form_state);
    //Add the Twitter API URL setting.
    $form['example_ext_rating_plugin_points'] = array(
        '#type' => 'textfield',
        '#title' => t('Rating points'),
        '#description' => t('Please enter the maximum number of rating points this plugin will always return.'),
        '#default_value' => isset($this->config['example_ext_rating_plugin_points'])?$this->config['example_ext_rating_plugin_points']:'',
        '#required' => TRUE,
    );
    return $form;
  }
  
  public function configFormValidate(&$values) {
    parent::configFormValidate($values);
    //Validate that the number of point is not smaller than 0.
    if ($values['example_ext_rating_plugin_points'] < 0 || !is_numeric($values['example_ext_rating_plugin_points'])){
      form_set_error('example_ext_rating_plugin_points', t('Please insert a valid and positive number'));
    }
  }
  
  public function getExternalRating(&$params) {
    foreach ($params as $key => $value){
      $params[$key]['rating_value'] = rand(1, $this->config['example_ext_rating_plugin_points']);
    }
    return TRUE;
  }
  
  /**
   * Returns the html widget for the plugin.
   */
  public function getHtml($params) {
    return "<div>Example plugin</div>";
  }
}